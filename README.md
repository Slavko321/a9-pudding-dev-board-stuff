# A9 Pudding dev board-stuff

A nice little board, with a 300mhz cpu, integrated GSM modem, optionally GPS and lots of connectivity.
https://ai-thinker-open.github.io/GPRS_C_SDK_DOC/en/hardware/pudding-dev-board.html

Using its SDK and sharing my experiences and solutions.

As the SDK is quite scarce, i hope my experiences will help you set it up faster.